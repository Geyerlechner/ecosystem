#pragma once
#include "Animals.h"

class Bear : public Animals {

public:
	int  power;

	Bear();
	Bear(char age);
	~Bear();
	char getSymbol();
		
private:
	int	 age;
	int maxAge = 9;
	int getMaxAge() { return maxAge; };
	char strength;

};
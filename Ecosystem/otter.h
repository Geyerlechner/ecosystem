#pragma once
#include "Animals.h"

class Otter : public Animals {

public:
	Otter();
	char getSymbol();
	int  getAge()	{ return age; }

private:
	int	 age;
	int  maxAge = 7;
	int  getMaxAge() { return maxAge; };
};
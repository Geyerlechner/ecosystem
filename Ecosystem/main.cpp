#include <iostream>

#include "Ecosystem.h"
#include "Fish.h"
#include "Animals.h"
#include "Otter.h"
#include "River.h"
#include "Bears.h"

int main()
{
    River eSys = River(200);
    int age = 0;

    // Add animals
    for (int i = 0; i < 60; i++)
        eSys.add(new Fish());
    
    for (int i = 0; i < 20; i++)
        eSys.add(new Otter());
    
    for (int i = 0; i < 20; i++) 
        eSys.add(new Bear());
    
    // Run simulation
    for (int i = 0; i < 50; i++) {
        eSys.simulate(1);
        std::cout << eSys.toCompactString() << std::endl;   
    }

    Fish f; Bear b; Otter o;
    std::cout << eSys.count(&f) << " Fish" << std::endl;
    std::cout << eSys.count(&o) << " Otter" << std::endl;
    std::cout << eSys.count(&b) << " Bear" << std::endl;
}
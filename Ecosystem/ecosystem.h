﻿#pragma once
#include "Animals.h"
#include <iostream>

class Ecosystem {
protected:
	void age();
private:
	
public:

	// Ecosystem();
	virtual void add(Animals* an) = 0;
	virtual void simulate(int step) = 0;
	virtual int count(Animals* an) = 0;
	virtual std::string toCompactString() = 0;
	
};

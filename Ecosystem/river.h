#pragma once
#include "Ecosystem.h"
#include "Animals.h"
#include <vector>

#define MAX_PERCENTAGE 60

class River : public Ecosystem {
private:
	std::vector<Animals*> m_river;
	int size;
	void moveAllAnimals();
	void age();

public:
	River() {};
	River(int size);
	~River();

	void add(Animals* an);
	void simulate(int step);
	void die();
	int moveAnimal(int fromPos, int toPos);
	
	int count(Animals* an);
	int m_counter;
	
	double maxPercentage;
	std::string toCompactString();
};
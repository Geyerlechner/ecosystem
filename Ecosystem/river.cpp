#include "River.h"
#include "Animals.h"
#include <typeinfo>
#include <cmath>
#include <ctime>

void River::moveAllAnimals()
{
	for (int i = 0; i < m_river.size(); i++)	
		if (m_river[i]) // !=NULL
		{
			int nextPos = rand() % 3 - 1;
			i = moveAnimal(i, i + nextPos);
		}
}

int River::moveAnimal(int fromPos, int toPos)
{
	Animals* a = m_river[fromPos];
	if (fromPos == toPos || toPos < 0 || toPos >= m_river.size()) return fromPos;
	Animals* other = m_river[toPos];
	if (!other) { // = NULL
		m_river[toPos] = a;
		m_river[fromPos] = NULL;
		return toPos;
	}
	// if same specier // down_cast / dynamic_cast
	// if same gender
	// else
	// else




}

void River::age()
{
	for (int i = 0; i < m_river.size(); i++)
	{
		if (m_river[i] && !m_river[i]->getOlder()) 
		{
			delete m_river.at(i);
			m_river[i] = NULL;
			m_counter--;
		}
	}
}

void River::die()
{

}

River::River(int size) : m_counter(0)
{
	srand(time(0));
	m_river.resize(size);
}

void River::add(Animals* an)
{
	if( m_counter >= m_river.size() ) return;
	
	if(count(an) >= MAX_PERCENTAGE / 100. * m_river.size()) return;
	
	int pos = rand() % m_river.size();
	while (m_river[pos] != NULL)
		pos = (pos + 1) % m_river.size();
		m_river[pos] = an;
		m_counter++;
}

River::~River()
{
	for (int i = 1; i < m_river.size(); i++)
		if (m_river[i] != NULL) delete m_river[i];
}

void River::simulate(int step)
{
	moveAllAnimals();
	age();
}

std::string River::toCompactString()
{
	std::string sRiver = "";
	for (int i = 1; i < m_river.size(); i++)
		if (m_river[i] != NULL)
			sRiver += m_river[i]->getSymbol();
		else
			sRiver += "-";

	return sRiver;
}

int River::count(Animals* an)
{
	int counter = 0;

	for (int i = 0; i < m_river.size(); i++)
		if (m_river[i] && typeid(*m_river[i]) == typeid(*an)) counter++;

	return counter;
}


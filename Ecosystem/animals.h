﻿#pragma once
#include <cmath>

#define MAX_PERCENTAGE 60;

class Animals
{
protected:
	bool female;
	int age;
	//float speciesStength;
	int maxAge;
	//char birthRate;
public:

	
	Animals() : age() {
		female = rand() % 2;
		// age = rand() % getMaxAge();
	};

	Animals(int a) : age(a) {};

	virtual int  getAge();
	virtual char getSymbol();
	virtual bool getOlder();
	virtual int  getMaxAge() = 0;

	// char getMaxAge() { return 9; };
	// static double maxPercentage = 60;
	// bool getFemale();
	// float getSpeciesStrength();
	// char getBirthRate();

};
#pragma once
#include "Animals.h"

class Fish : public Animals {

public:
	Fish() {};
	char getSymbol();
	int  getAge()	 { return age; };

private:

	int age;
	int maxAge = 5;
	int getMaxAge() { return maxAge; };
};